import { Rect } from './ts/classes/Rect';
import { Circle } from './ts/classes/Circle';
import { Ellipse } from './ts/classes/Ellipse';
import { Canvas } from './ts/classes/Canvas';
import { CircleTable } from './ts/classes/CircleTable';

const btn = document.getElementById('button');
const canvas = <HTMLCanvasElement>document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const circleTable = document.getElementById('circleTable');

const width: number = 400;
const height: number = 400;

canvas.width = width;
canvas.height = height;

btn.addEventListener('click', () => {
  console.clear();

  const canvas1: Canvas = new Canvas(ctx);
  canvas1.cleanCanvas(width, height);

  const rect1: Rect = new Rect();
  const rect2: Rect = new Rect();

  rect1.getRandomProperties(width, height);
  rect2.getRandomProperties(width, height);

  canvas1.drawRect(rect1, 'blue');
  canvas1.drawRect(rect2, 'green');

  CircleTable.cleanTable(circleTable);

  if (rect1.isIntersection(rect2)) {
    const innerRect: Rect = rect1.getRectIntersection(rect2);
    canvas1.drawRect(innerRect, 'orange');

    const innerEllipse: Ellipse = new Ellipse();
    innerEllipse.getParameters(innerRect);
    canvas1.drawEllipse(innerEllipse, 'red');

    const circleArray: Circle[] = [];

    for (let i = 0; i < 10; i++) {
      circleArray[i] = new Circle();
      circleArray[i].getParameters(innerRect);
      canvas1.drawCircle(circleArray[i], 'brown');
    }

    CircleTable.putCoordinatesInTable(circleArray, circleTable);

    console.log(circleArray);
    console.log(innerEllipse);
    console.log(innerRect);
  }

  console.log(rect1);
  console.log(rect2);
});
