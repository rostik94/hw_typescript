import { Rect } from './Rect';
import { Ellipse } from './Ellipse';
import { Circle } from './Circle';

export class Canvas {
  constructor(public ctx: CanvasRenderingContext2D) {
    this.ctx = ctx;
  }

  cleanCanvas(width: number, height: number): void {
    this.ctx.clearRect(0, 0, width, height);
  }

  drawRect(rect: Rect, color: string): void {
    this.ctx.beginPath();
    this.ctx.strokeStyle = color;
    this.ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
    this.ctx.closePath();
  }

  drawEllipse(ellipse: Ellipse, color: string): void {
    this.ctx.beginPath();
    this.ctx.strokeStyle = color;
    this.ctx.ellipse(
      ellipse.x,
      ellipse.y,
      ellipse.width,
      ellipse.height,
      0,
      0,
      2 * Math.PI
    );
    this.ctx.stroke();
    this.ctx.fillStyle = 'rgba(0,0,0,0.4)';
    this.ctx.fill();
    this.ctx.closePath();
  }

  drawCircle(circle: Circle, color: string): void {
    this.ctx.beginPath();
    this.ctx.strokeStyle = color;
    this.ctx.arc(circle.x, circle.y, 2, 0, 2 * Math.PI);
    this.ctx.stroke();
  }
}
