import { IPoint } from '../interfaces/IPoint';
import { Rect } from './Rect';

export class Circle implements IPoint {
  x: number;
  y: number;

  getParameters(innerRect: Rect): void {
    const randomAngle: number = Math.random() * 2 * Math.PI;
    const randomX: number =
      (((Math.random() * innerRect.width) / 2) * innerRect.width) / 2;
    const randomY: number =
      (((Math.random() * innerRect.height) / 2) * innerRect.height) / 2;
    const dotX: number = Math.sqrt(randomX) * Math.cos(randomAngle);
    const dotY: number = Math.sqrt(randomY) * Math.sin(randomAngle);
    const pointX: number = dotX + innerRect.x + innerRect.width / 2;
    const pointY: number = dotY + innerRect.y + innerRect.height / 2;

    this.x = Math.round(pointX);
    this.y = Math.round(pointY);
  }
}
