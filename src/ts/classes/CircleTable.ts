import { Circle } from './Circle';
import { coordinatesTemplates } from '../templates/coordinatesTemplates';

export class CircleTable {
  static putCoordinatesInTable(
    circleArray: Circle[],
    tableTag: HTMLElement
  ): void {
    tableTag.insertAdjacentHTML('afterbegin', '<tr><th>X</th><th>Y</th></tr>');
    circleArray.forEach((circle: Circle) => {
      const template = coordinatesTemplates(circle);
      tableTag.insertAdjacentHTML('beforeend', template);
    });
  }

  static cleanTable(tableTag: HTMLElement): void {
    tableTag.textContent = '';
  }
}
