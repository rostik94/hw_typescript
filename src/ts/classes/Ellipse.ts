import { IPoint } from '../interfaces/IPoint';
import { Rect } from './Rect';

export class Ellipse implements IPoint {
  x: number;
  y: number;
  width: number;
  height: number;

  getParameters(innerRect: Rect): void {
    this.x = innerRect.x + innerRect.width / 2;
    this.y = innerRect.y + innerRect.height / 2;
    this.width = innerRect.width / 2;
    this.height = innerRect.height / 2;
  }
}
