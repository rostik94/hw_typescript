import { IRect } from '../interfaces/IRect';

export class Rect implements IRect {
  x: number;
  y: number;
  width: number;
  height: number;

  constructor();
  constructor(x: number, y: number, width: number, height: number);
  constructor(x?: number, y?: number, width?: number, height?: number) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  getRandomProperties(maxWidth: number, maxHeight: number): void {
    this.x = Math.round(Math.random() * maxWidth);
    this.y = Math.round(Math.random() * maxHeight);
    this.width = Math.round(Math.random() * (maxWidth - this.x));
    this.height = Math.round(Math.random() * (maxHeight - this.y));
  }

  getRectIntersection(rect: Rect): Rect {
    const intersRect: any = {};

    if (this.x >= rect.x) {
      intersRect.x = this.x;

      if (
        this.width < rect.width &&
        this.x + this.width < rect.x + rect.width
      ) {
        intersRect.width = this.width;
      } else {
        intersRect.width = rect.x + rect.width - intersRect.x;
      }
    } else if (this.x < rect.x) {
      intersRect.x = rect.x;

      if (
        rect.width < this.width &&
        rect.x + rect.width < this.x + this.width
      ) {
        intersRect.width = rect.width;
      } else {
        intersRect.width = this.x + this.width - intersRect.x;
      }
    }

    if (this.y >= rect.y) {
      intersRect.y = this.y;

      if (
        this.height < rect.height &&
        this.y + this.height < rect.y + rect.height
      ) {
        intersRect.height = this.height;
      } else {
        intersRect.height = rect.y + rect.height - intersRect.y;
      }
    } else if (this.y < rect.y) {
      intersRect.y = rect.y;

      if (
        rect.height < this.height &&
        rect.y + rect.height < this.y + this.height
      ) {
        intersRect.height = rect.height;
      } else {
        intersRect.height = this.y + this.height - intersRect.y;
      }
    }

    return new Rect(
      intersRect.x,
      intersRect.y,
      intersRect.width,
      intersRect.height
    );
  }

  isIntersection(rect: Rect): boolean {
    if (
      rect.x <= this.x + this.width &&
      this.x <= rect.x + rect.width &&
      rect.y <= this.y + this.height &&
      this.y <= rect.y + rect.height
    ) {
      return true;
    }
  }
}
