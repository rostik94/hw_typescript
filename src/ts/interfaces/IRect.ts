import { IPoint } from './IPoint';

export interface IRect extends IPoint {
  width: number;
  height: number;
}
