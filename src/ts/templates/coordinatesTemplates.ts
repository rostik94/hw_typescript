import { Circle } from '../classes/Circle';

export const coordinatesTemplates = (circle: Circle): string => {
  return `<tr>
            <td>${circle.x}</td>
            <td>${circle.y}</td>
          </tr>`;
};
